"""
Driver Group coding challenge.

input: up to 50 DNA sequences up to 1000 bases in length. Each base will be
T/C/G/A 
There will be overlapping among sequences such that every sequence will
overlap another by at least half of its length. 
output: the constructed sequence after aligning the overlaps

approach: first, read in all the fragments.

Starting with the first fragment (could pick any), 
compare with all other fragments to find
an overlapping fragment on each side. With the pre-fragment, continuously
search for fragments until none are found. Either you'll find them all
and create one long string, or you'll quit after enough iterations.
"""
import sys

def check_front_alignment(astring, fragment, index):
    """Test whether astring ends with the first part
    of the fragment.
    index is the number of characters in astring before
    we encounter the first character from fragment
    If it aligns, return the aligned string, otherwise
    return None
    """
    end = len(astring) - index
    # If the last bit of astring matches the first
    # bit of the fragment
    if astring[index:] == fragment[:end]:
        # return the whole astring plus the parts
        # of the fragment that weren't matching
        return astring + fragment[end:]
    return None

def check_rear_alignment(astring, fragment, index):
    """Test whether astring starts with
    the end of the fragment.
    index is the number of characters overlapping
    If it aligns, return the aligned string, otherwise
    return None
    """
    # If the last bit of fragment matches the first bit
    # of astring
    if fragment[-index:] == astring[:index]:
        # return the whole fragment plus the part of
        # astring that wasn't the matching bits at the
        # front
        return fragment + astring[-(len(astring)-index):]
    return None


def split_fragment(frag):
    """Split a string fragment in half, and return the two halves.
    If the string is of odd length, we ignore the middle character.
    """
    mid = len(frag) / 2
    if len(frag) % 2 == 0:
        return frag[:mid], frag[mid:]
    else:
        return frag[:mid], frag[mid+1:]


def compare_fragment(astring, fragment):
    """Given astring and a fragment, see if they can be aligned
    If they can, return the new string formed, otherwise, None
    """
    front_half, back_half = split_fragment(fragment)
    front_idx = astring.find(front_half)
    back_idx = astring.find(back_half)
    if front_idx != -1:
        aligned_string = check_front_alignment(astring, fragment, front_idx)
        if aligned_string:
            return aligned_string
    if back_idx != -1:
        back_idx += len(back_half)
        aligned_string = check_rear_alignment(astring, fragment, back_idx)
        if aligned_string:
            return aligned_string
    return None


def get_filename():
    """Get the filename from the command line, default to the one supplied
    by Driver.
    """
    try:
        filename = sys.argv[1]
    except IndexError:
        return "coding_challenge_data_set.txt"
    return filename


def read_input(filename, frag_dict):
    """Given the filename to read in, write the fragments to the
    frag_dict dictionary.
    """
    with open(filename) as a_file:
        content = ""
        fragment_name = ""
        for line in a_file.readlines():
            if line.startswith(">"):
                if content != "":
                    frag_dict[fragment_name] = content
                fragment_name = line[1:].strip()
                content = ""
            else:
                content += line.strip()
        frag_dict[fragment_name] = content


if __name__ == '__main__':
    """Program starts here."""
    fragments = {}
    read_input(get_filename(), fragments)
    first_key = fragments.keys()[0]
    try:
        my_string = fragments.pop(first_key)
    except KeyError:
        print "wow, never expected that!"
        quit(1)
    to_delete = []
    iterations = 0
    max_iterations = len(fragments)
    while len(fragments) > 0 and iterations < max_iterations:
        for fragment in fragments:
            new_string = compare_fragment(my_string, fragments[fragment])
            if new_string:
                to_delete.append(fragment)
                my_string = new_string
        for fragment in to_delete:
            fragments.pop(fragment)
        to_delete = []
        iterations += 1
    print my_string
