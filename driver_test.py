"""Unit test some functions."""
import driver

f, o = driver.split_fragment('foo')
assert f == "f"
assert o == "o"

fo, od = driver.split_fragment('food')
assert fo == "fo"
assert od == "od"

fr, es = driver.split_fragment('fries')
assert fr == "fr"
assert es == "es"

bur, ger = driver.split_fragment('burger')
assert bur == "bur"
assert ger == "ger"

result1 = driver.compare_fragment('ATTAGACCTG', 'AGACCTGCCG')
assert result1 == 'ATTAGACCTGCCG'


result2 = driver.check_rear_alignment('GCCGGAATAC', 'CCTGCCGGAA', 7)
#    GCCGGAATAC
# CCTGCCGGAA
assert result2 == 'CCTGCCGGAATAC'

# Looking for CGGAA in GCCGGAATAC
# GCCGGAATAC
#   CGGAA

result3 = driver.compare_fragment('GCCGGAATAC', 'CCTGCCGGAA')
# print "result3 = " + str(result3)
assert result3 == 'CCTGCCGGAATAC'

result4 = driver.check_front_alignment("abcdefg", "cdefghi", 2)
assert result4 == 'abcdefghi'

result5 = driver.check_front_alignment("abcdefg", "cde1234", 2)
assert result5 == None

result6 = driver.check_rear_alignment("abcdefg", "12abcde", 5)
assert result6 == "12abcdefg"


result7 = driver.compare_fragment('CCTGCCGGAATAC', 'AGACCTGCCG')
#    CCTGCCGGAATAC
#      TGCCG
# AGACCTGCCG
# print "result7 = " + str(result7)
assert result7 == "AGACCTGCCGGAATAC"

result8 = driver.compare_fragment('CCTGCCGGAATAC', 'ATTAGACCTG')
# print result8
#       CCTGCCGGAATAC
# ATTAGACCTG
assert result8 is None

result9 = driver.check_rear_alignment('CCTGCCGGAATAC', 'AGACCTGCCG', 7)
# print "result9 = " + str(result9)
assert result9 == "AGACCTGCCGGAATAC"

result10 = driver.compare_fragment('1234567890', 'abc1234')
# print "result10 = " + str(result10)
assert result10 == "abc1234567890"

result11 = driver.compare_fragment('1234567890', '890Xabc')
# print "result11 = " + str(result11)
assert result11 == "1234567890Xabc"

result12 = driver.compare_fragment('AGACCTGCCGGAATACGATATTAAATT', 'ATTAGACCTG')
# print result12
assert result12 == 'ATTAGACCTGCCGGAATACGATATTAAATT'
#    AGACCTGCCGGAATACGATATTAAATT
# ATTAGACCTG

print "Success!"
